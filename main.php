<?php
// use Monolog\Handler\ExceptionTestHandler;
// include("c:/xampp/htdocs/wt-algorithms/loader.php");

error_reporting(E_ALL);

if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
    die("Please run 'composer install'");
} else {
    require_once __DIR__ . '/vendor/autoload.php';
}
if (!file_exists(__DIR__ . '/conf.php')) {
    die("Please create enviroment conf");
} else {
    require_once __DIR__ . '/conf.php';
}


// $dbo = new MysqliDb (
//     $mysql_host,
//     $mysql_user,
//     $mysql_pass,
//     $mysql_db
// );
DEFINE ("MYSQL_DB", $mysql_db);
$dboc = [
    'mysql_host' => $mysql_host,
    'mysql_user' => $mysql_user,
    'mysql_pass' => $mysql_pass,
    'mysql_db'   => $mysql_db
];


class DataChomper {
    private $db;
    private $view;
    private $activetask;
    private $taskinfo;
    private $tasklist;
    private $tasksfilepath;
    private $taskoutput;
    private $tmp_taskoutput;
    private $execution_time;
    private $tmpmode;
    private $WTA;

    function __construct($dboc, $activetask='') {
        
        // $this->db = $dbo;
        $this->tasksfilepath = __DIR__ . '/tasks/';
        $this->toolsfilepath = __DIR__ . '/tasks/toolbox/';
        $this->tasklist = array();
        $this->activetask = $activetask;
        $this->populateTaskList();
        $this->view = 'index';
        $this->execution_time = -1;
        $this->taskoutput = "";
        $this->tmp_taskoutput = "";
        $this->tmpmode = 0;
        
        $this->db = new MysqliDb (
            $dboc['mysql_host'],
            $dboc['mysql_user'],
            $dboc['mysql_pass'],
            $dboc['mysql_db']
        );

        $test = $this->db->rawQueryOne('select ID from wp_posts LIMIT 1');
        if (empty($test)) {
            die("Could not find any posts");
        }
        $this->WTA = new WT_Algorithms(
            $dboc['mysql_host'],
            $dboc['mysql_user'],
            $dboc['mysql_pass'],
            $dboc['mysql_db']
        );
        require_once ("tasks/task_log_setup.php");
        // die("test");
        if (!empty($this->activetask)) {
            if ($this->activetask == '_all-tasks') {
                $this->runAllTasks();
            } else {
                $this->runActiveTask();
            }
        } 
        
    }
    public function html () {
        $filepath = __DIR__ . '/html/'.$this->view.'.php';
        if (file_exists($filepath)) {
            
            $taskinfo = $this->getActiveTaskInfo();
            $taskoutput = $this->filteredTaskOutput();
            
            ob_start();
            include($filepath);
            $output = ob_get_clean();
            return($output);
        } else {
            $o = 'ERROR: ';
            $o .= $filepath.'<br>';
            $o .= "html not found";
            return ($o);
        }
    }
    public function getTaskTimeElapsed() {
        $t = round($this->execution_time ,4);
        return ($t);
    }
    public function unfilteredTaskOutput () {
        return($this->taskoutput);
    }
    public function filteredTaskOutput() {
        $output = preg_replace('/\n(.+?):\s?\n/',"\n<u>$1:</u>\n",$this->taskoutput);
        $output = preg_replace('/\n?\*(.+?)\*\s?\n/',"\n<strong>$1</strong>\n",$output);
        $output = preg_replace('/!!(.+?)\!!\s?\n/',"<em>$1</em>\n",$output);
        $output = preg_replace('/\n===(.+?)===\s?\n/',"\n<h3>$1</h3>\n",$output);
        $output = preg_replace('/\n---(.+?)---\s?\n/',"\n<h4>$1</h4>\n",$output);
        
        return($output);
    }
    public function getTaskList () {
        return($this->tasklist);
    }
    private function getActiveTaskInfo () {
        return($this->getTaskInfo($this->activetask));  
    }
    private function getActiveTaskDir () {
        return($this->getTaskDir($this->activetask));  
    }
    private function getTaskDir($task_name) {
        $task_dir = $this->tasksfilepath .$task_name.'/';
        if (is_dir($task_dir)) {
            return($task_dir);
        } else {
            return false;
        }
    }
    private function runActiveTask() {
        $this->runSingleTask($this->activetask);
    }
    private function appendOutput($output) {
        if (!$this->tmpmode) {
            $this->taskoutput .= "\n".$output;
        } else {
            $this->tmp_taskoutput .= "\n".$output;
        }
    }
    private function prependOutput($output) {
        echo "addTaskLog [".boolval($this->tmpmode)."]<br>";
        if (!$this->tmpmode) {
            $this->taskoutput = $output."\n".$this->taskoutput;
        } else {
            $this->tmp_taskoutput = $output."\n".$this->tmp_taskoutput;
        }
    }
    private function addTaskLog($task) {
        $starttime = microtime(true);
        $o = (!$this->tmpmode) ? $this->taskoutput : $this->tmp_taskoutput;
        if (empty($o)) {
            $o = '--BLANK [ADD]--';
        }
        $log_id = $this->db->insert('dc_task_log', array(
            'task_name'         =>  $task,
            'start_datetime'    =>  date('Y-m-d H:i:s', $starttime),
            'end_datetime'      =>  '1000-01-01 00:00:00',
            'output'            =>  $o,
            'success'           =>  0
        ));
        return($log_id);
    }
    private function markLog ($log_id, $success = 1) {
        // echo 'Log ID : '.$log_id.'<br>';
        $o = (!$this->tmpmode) ? $this->taskoutput : $this->tmp_taskoutput;
        // print_r($o);


        if (empty($o)) {
            $o = '--BLANK [EDIT]--';
            die('EMPTY O:'.$this->tmpmode.'<hr>'.$this->taskoutput.'<hr>'.$this->tmp_taskoutput);
        }
        $endtime = microtime(true);

        // print_r($endtime);
        $this->db->where('log_id',$log_id);
        $this->db->update('dc_task_log', array(
            'end_datetime'=>date('Y-m-d H:i:s', $endtime),
            'success'=>$success,
            'output'  => $o,
        ));
    }
    private function pruneLog($span = "-3 days") {
        $time = strtotime($span);
        $q = "DELETE FROM `dc_task_log` WHERE `dc_task_log`.`start_datetime`  < '".date("Y-m-d H:i:s", $time)."'";
        $this->db->rawQuery($q);
        $this->appendOutput("Removed task log entries before: ".date("Y-m-d H:i:s", $time));
    }
    private function appendExecutionTimeToOutput() {
        $seconds = round($this->execution_time, 5);
        if ($seconds > 60) {
            $mins = floor($this->execution_time / 60);
            $secs = round($this->execution_time - ($mins * 60));
            $time = $mins.' min, '.$secs.' sec ['.$seconds.' seconds]';
        } else {
            $time = $seconds." seconds";
        }
        $this->appendOutput("Execution time: ".$time);
        
    }
    private function checkTableExists ($tablename) {
        $q = "SELECT *
        FROM information_schema.tables
        WHERE table_schema = '".MYSQL_DB."'
            AND table_name = '$tablename'
        LIMIT 1;";
        
        $r = $this->db->rawQuery($q);
        if (empty($r)) {
            return false;
        } else {
            return true;
        }
    }
    private function getTaskSQLFiles () {
        $return = array();
        $files = array_diff(scandir($this->getTaskDir( $this->taskinfo ->task_name)), array('..', '.'));
        foreach ($files as $f) {
            if (preg_match('/^sql\.(.+?)\.php$/', $f)) {
                $n = explode('.' ,  $f);
                if (!empty($n[1])) {
                    $return[$n[1]] = $f;
                    continue;
                } 
            }
        }
        return ($return);
    }
    private function createTaskTables() {
        $files = $this->getTaskSQLFiles( $this->taskinfo );
        if (!empty($files)) {
            $this->appendOutput("Checking task DB tables");
            foreach ($files as $n => $f) {
                $f = $this->getTaskDir($this->taskinfo->task_name).$f;;
                $names = array('_dc_'.$n, 'dc_'.$n);
                foreach ($names as $tablename) {
                    if (!$this->checkTableExists($tablename)) {
                        $this->appendOutput($tablename." will be created");
                        include($f);
                        if (!empty($q)) {
                            foreach ($q as $l) {
                                if (!empty($l)) {
                                    // echo '<pre>'.$l.'</pre><br>';
                                    $this->db->rawQuery($l);
                                    $error = $this->db->getLastError();
                                    if (!empty($error)) {
                                        $this->appendOutput('ERROR: '.$error);
                                    }
                                }
                            }
                        }
                        // echo $this->taskoutput;
                        // exit;
                    } else {
                        $this->appendOutput($tablename." exists");
                    }
                }
            }
        }
    }
    private function swapAndDropTables () {
        $files = $this->getTaskSQLFiles( $this->taskinfo );
        if (!empty($files)) {
            $this->appendOutput( "Swapping and dropping data");
            foreach ($files as $n => $f) {
                $drop = 'dc_'.$n;
                $swap = '_dc_'.$n;
                $this->appendOutput($drop." will be replaced");
                $this->db->rawQuery("DROP TABLE `$drop`");
                $this->db->rawQuery("RENAME TABLE `$swap` TO `$drop`;");
            }
        }
        
    }
    private function setTaskInfo($taskinfo) {
        $this->taskinfo = $taskinfo;
    }
    private function runTask($taskinfo) {
        $this->setTaskInfo($taskinfo);
        $this->createTaskTables();
        
        $taskfile = $this->getTaskDir( $this->taskinfo->task_name).'task.php';
//         if ($this->tmpmode) {
//             die("Yes");
//         } else {
//             die("No");
//         }
        if (file_exists($taskfile)) {
        
            $starttime = microtime(true);
            $tl_log_id = $this->addTaskLog( $this->taskinfo->task_name);
            if (empty($tl_log_id)) {
                $this->appendOutput('Error logging:'.$this->db->getLastError() );
                return false;
            }
            
            ob_start();
            $this->appendOutput("Setting time_limit to ". $this->taskinfo ->maxtime." seconds..." );
            
            set_time_limit( $this->taskinfo ->maxtime);
            $success = 0;
            try {
                $success = true;
                include($taskfile);
                $this->appendOutput(ob_get_clean() );
                $success = 1;
            } catch (Exception $e) {
                $this->appendOutput(
                    "ERROR: ".$e->getMessage()
                    ."\n\n\/\/\/\\\n\n"
                    .ob_get_clean() );
            }
            $endtime = microtime(true);
            $this->execution_time = $endtime - $starttime;
            $this->appendExecutionTimeToOutput();
            
            $this->appendOutput(round(($this->execution_time/ $this->taskinfo ->maxtime)*100,2)."% capacity of maxtime" );

            // echo '<br>--DONE &gt; MarkLog ---<br>';
            $this->markLog($tl_log_id, $success);

             // die("success: ".$success);
            if ($success) {
                $this->swapAndDropTables();
            } else {
                $this->appendOutput("Task failed. No data will be copied");
            }
        } else {
            $this->appendOutput("Task file does not exist [$taskfile]", $tmp );
        }
        
    }
    private function runAllTasks () {
        
        $this->appendOutput("DC_ENV: ".DC_ENV);
        $this->appendOutput("Attempting to run all tasks");
        $totaltime = 0;
        foreach ($this->tasklist as $t => $task) {
            $taskinfo = $this->getTaskInfo($t);
            if (preg_match('/^[0-9]+$/',$taskinfo->maxtime)) {
                $totaltime += $task->maxtime;
            } else {
                $this->appendOutput("Invalid timeout for task [$t]");
                return false;
            }
        }
        $this->appendOutput("Setting max_execution_time set to ".$totaltime." seconds...");
        $this->appendOutput("(or ".($totaltime/60)." mins)");
        ini_set('max_execution_time', $totaltime);
        //die($this->taskoutput);
        
        $all_logid = $this->addTaskLog('AllTasks');
        if (empty($all_logid)) {
            $this->appendOutput('Error logging:'.$this->db->getLastError());
            return false;
        }
        $combinedoutput = "";
        $this->tmpmode = 1;
        foreach ($this->tasklist as $task_name => $task) {
            $this->tmp_taskoutput = "";
           
            $taskinfo = $this->getTaskInfo($task_name);
            $this->appendOutput(" : : ".$taskinfo->name." : : ");
            $this->runTask($taskinfo);
            $combinedoutput .= $this->tmp_taskoutput;
        }
        $this->tmpmode = 0;
        $this->markLog($all_logid, 1);
        $this->appendOutput($combinedoutput);
        $this->pruneLog();
    }
    private function runSingleTask($task_name) {
        $this->appendOutput("DC_ENV: ".DC_ENV);
        $taskinfo = $this->getTaskInfo($task_name);
        $this->appendOutput(" : : ".$taskinfo->name." : : ");
        $this->appendOutput("Setting max_execution_time set to ".$taskinfo->maxtime." seconds...");
        ini_set('max_execution_time', $taskinfo->maxtime);
        $this->tmpmode = 0;
        $this->runTask($taskinfo, false);
        $this->pruneLog();
    }
    private function getTaskInfo($task_name) {
        // If this is a proper dir =
        // Check for info.json exists
        $task_dir = $this->getTaskDir($task_name);
        // echo $task_dir;
        if (is_dir($task_dir))
        {
            $infofile = $task_dir.'/info.json';
            if (file_exists($task_dir.'/info.json')) {
                // Decode the contents
                $c = file_get_contents($task_dir.'/info.json');
                $info = json_decode($c);
                if (empty($info)) {
                    echo 'Empty info for '.$task_name;
                    return($info);
                }
                $info->dir = $task_dir;
                $info->task_name = $task_name;
                if  (json_last_error() == JSON_ERROR_NONE) {
                    // We did it!
                    return($info);
                }
            } 
        }
        return false;
    }
    private function populateTaskList () {
        
        // Check the tasks folder is there
        if (file_exists($this->tasksfilepath)) {
            // Get the folder contents
            $taskfoldercontents = scandir($this->tasksfilepath);
            // Loop over sub-folders contents
            $x = 1;
            foreach ($taskfoldercontents as $sfolder)
            {
                // Ignore the silly dots
                if (!in_array($sfolder,array(".","..")))
                {
                    $info = $this->getTaskInfo($sfolder);
                    if ((!empty($info)) && (!empty($info->env))) {
                        if (!in_array(DC_ENV, $info->env)) {
                            continue;
                        }
                        $info->sfolder = $sfolder;
                        $order = (isset($info->order)) ? $info->order : 0;
                        $this->tasklist[$order.'.'.$x] = $info;
                    } else if (!empty($info)) {
                        print_r($info);
                        echo "*** The task ".$info->name." does not have an env set ***"."\n";
                    }
                }
                $x++;
            }
            
            ksort($this->tasklist);
            foreach ($this->tasklist as $k => $t) {
                unset($this->tasklist[$k]);
                $this->tasklist[$t->sfolder] = $t;
            }
            
        }
                    
    }
    private function get_job_list_post_ids($task_name="") {
        if (empty($task_name)) {
            $task_name = $this->activetask;
        }
        $q = "SELECT post_id FROM dc_job_list WHERE task_name = '$task_name'";
        $r = $this->db->rawQuery($q);
        $post_ids = array();
        foreach ($r as $p) {
            $post_ids[] = $p['post_id'];
        }
        return($post_ids);
    }
}