<?php
class wptools {
    private $db;
    private $post_id;
    function __construct($dbo) {
        $this->db = $dbo;
    }
    function getAllPublishedProjects ($cols=array(), $limit = 200, $orderby = "") {
        if (is_string($cols)) {
            $cols = $cols;
        } else {
            $cols = (empty($cols)) ? '*' : implode(',', $cols);
        }
        $q = "
            SELECT $cols 
            FROM wp_posts 
            WHERE post_type = 'projects' 
                AND post_status = 'publish' 
                AND post_parent = 0";
        
      
        
        if (!empty($orderby)) {
            $q .= " ORDER by ".$orderby;
        }
        if ($limit > 0) {
            $q .= " LIMIT ".$limit;
        }
        
        return($this->db->rawQuery($q));
    }
    function getAllPublishedStories ($cols=array(), $limit = 200, $orderby = "") {
        if (is_string($cols)) {
            $cols = $cols;
        } else {
            $cols = (empty($cols)) ? '*' : implode(',', $cols);
        }
        $q = "
            SELECT $cols 
            FROM wp_posts 
            WHERE post_type = 'stories' 
                AND post_status = 'publish' 
                AND post_parent = 0";
        
      
        
        if (!empty($orderby)) {
            $q .= " ORDER by ".$orderby;
        }
        if ($limit > 0) {
            $q .= " LIMIT ".$limit;
        }
        
        return($this->db->rawQuery($q));
    }
    function getAllPublishedStoriesAndProjects ($cols=array(), $limit = 200, $orderby = "") {
        if (is_string($cols)) {
            $cols = $cols;
        } else {
            $cols = (empty($cols)) ? '*' : implode(',', $cols);
        }
        $q = "
            SELECT $cols 
            FROM wp_posts 
            WHERE post_type IN ('stories', 'projects') 
                AND post_status IN  ('publish') 
                AND post_parent = 0";
        
      
        
        if (!empty($orderby)) {
            $q .= " ORDER by ".$orderby;
        }
        if ($limit > 0) {
            $q .= " LIMIT ".$limit;
        }
        
        return($this->db->rawQuery($q));
    }
    function getAllPublishedAndDraftStoriesAndProjects ($cols=array(), $limit = 200, $orderby = "") {
        if (is_string($cols)) {
            $cols = $cols;
        } else {
            $cols = (empty($cols)) ? '*' : implode(',', $cols);
        }
        $q = "
            SELECT $cols 
            FROM wp_posts 
            WHERE post_type IN ('stories', 'projects') 
                AND post_status IN  ('draft','publish') 
                AND post_parent = 0";
        
      
        
        if (!empty($orderby)) {
            $q .= " ORDER by ".$orderby;
        }
        if ($limit > 0) {
            $q .= " LIMIT ".$limit;
        }
        
        return($this->db->rawQuery($q));
    }
    function getAllTaxonomy($cols=array(), $limit = 200) {
        if (is_string($cols)) {
            $cols = $cols;
        } else if (empty($cols)) {
            $cols = "wp_terms.*";   
        } else {
            $cols =  implode(',', $cols);
        }
        $q = "
            SELECT term_id, $cols
            FROM wp_terms";
        
        if ($limit > 0) {
            $q .= " LIMIT ".$limit;
        }
//         echo $q;
        $r = $this->db->rawQuery($q);
        $results = array();
        foreach ($r as $t) {
            $results[$t['term_id']] = $t;
        }
        return($results);
    }
    public function setPostId($post_id) {
        if (is_numeric($post_id)) {
            $this->post_id = $post_id;
        } else {
            die("post_id is not numeric");
        }
    }
    public function getStoryPrimaryCategory ($justtermid = false) {
        $cols = ($justtermid) ? 't.term_id' : 't.term_id, parent, name, slug, count';
        return($this->db->rawQuery( "
            SELECT  $cols
            FROM `wp_postmeta` pm
            LEFT JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id  = pm.meta_value
            LEFT JOIN wp_terms t ON t.term_id = tt.term_id
            
            WHERE post_id = $this->post_id AND meta_key =  'primary_topic'
        "));
    }
    public function getPostCategories ($justtermid = false) {
        $cols = ($justtermid) ? 't.term_id' : 't.term_id, parent, name, slug, count';
        return($this->db->rawQuery("
            SELECT  $cols
            FROM `wp_term_relationships` tr

            LEFT JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id  = tr.term_taxonomy_id
            LEFT JOIN wp_terms t ON t.term_id = tt.term_id

            WHERE object_id = $this->post_id AND taxonomy =  'category'
        "));    
    }
    public function getPostTags ($justtermid = false) {
        $cols = ($justtermid) ? 't.term_id' : 't.term_id, parent, name, slug, count';
        
        return($this->db->rawQuery("
            SELECT  $cols
            FROM `wp_term_relationships` tr

            LEFT JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id  = tr.term_taxonomy_id
            LEFT JOIN wp_terms t ON t.term_id = tt.term_id

            WHERE object_id = $this->post_id AND taxonomy =  'post_tag'
        "));    
    }
    public function getPostContent () {
        $r = $this->db->rawQuery("
            SELECT  post_content FROM wp_posts
            WHERE ID = $this->post_id LIMIT 1
        ");
        $r = reset($r);
        if (!empty($r)) {
            return($r['post_content']);    
        }
    }
    public function getPostTitle () {
        $r = $this->db->rawQuery("
            SELECT  post_title FROM wp_posts
            WHERE ID = $this->post_id LIMIT 1
        ");
        $r = reset($r);
        if (!empty($r)) {
            return($r['post_title']);    
        }
    }
    public function getOption ($option_name) {
        $r = $this->db->rawQuery("
            SELECT  option_value FROM wp_options
            WHERE option_name = '$option_name' LIMIT 1
        ");
        $r = reset($r);
        if (!empty($r)) {
            return($r['option_value']);    
        }
    }
    public function getPostMeta($post_id, $meta_name) {
        $r = $this->db->rawQuery("
            SELECT * FROM `wp_postmeta` WHERE `post_id` = '$post_id' AND `meta_key` LIKE '$meta_name' LIMIT 1
        ");
        $r = reset($r);
        if (!empty($r)) {
            return($r['meta_value']);    
        }
    }
    public function getLastRevisionForPost($post_id) {
        $q = "select * from `wp_posts` 
             WHERE
             post_type = 'revision'
             AND post_parent = ".$post_id."
             AND post_name LIKE '".$post_id."-revision-v1'
             ORDER by post_date DESC, ID  DESC 
             LIMIT 1
             ";
              // echo $q;
        $r= $this->db->rawQuery($q);
        if (!empty($r)) {
            return($r[0]);
        } else {
            return;
        }
    }

    public function getAcceptedRevisionForPost($post_id) {

        $accepted_id = $this->getPostMeta($post_id, '_fpr_accepted_revision_id');

        $q = "select * from `wp_posts` 
             WHERE
             post_type = 'revision'
             AND post_parent = ".$post_id."
             AND ID = '".$accepted_id."'
             ORDER by post_date DESC, ID  DESC 
             LIMIT 1
             ";
              // echo $q;
        $r= $this->db->rawQuery($q);
        if (!empty($r)) {
            return($r[0]);
        } else {
            return;
        }
    }

}