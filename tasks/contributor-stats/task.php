<?php
// use function GuzzleHttp\json_encode;

$maxcontributorsperpost = 3;

function createCSTable($dbo, $preload = false, $onlyifnotexists = true) {
    $tablename = (!$preload) ? 'dc_post_contributions' : '_dc_post_contributions';
    
    if ($onlyifnotexists) {
        $q = "SELECT *
        FROM information_schema.tables
        WHERE table_schema = 'wikitribune'
            AND table_name = '$tablename'
        LIMIT 1;";
        
        $r = $dbo->rawQuery($q);
        if (!empty($r)) {
            return false;
        }
        
    }
    $cq[] = "CREATE TABLE `$tablename` (
  `rel_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `contr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=".DB_CHARSET.";";
    $cq[] = "ALTER TABLE `$tablename`
  ADD PRIMARY KEY (`rel_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);";
    
    $cq[] = "ALTER TABLE `$tablename`
  MODIFY `rel_id` bigint(20) NOT NULL AUTO_INCREMENT;";
    $cq[] = "COMMIT;";
    
    foreach ($cq as $q) {
        $dbo->rawQuery($q);
    }
}
createCSTable($this->db);
createCSTable($this->db, true);





$q = '
SELECT
    p.post_author, p.ID, p.post_parent, display_name
FROM wp_posts p
LEFT JOIN wp_users u on p.post_author = u.ID
LEFT JOIN wp_posts p2 on p.post_parent = p2.ID
WHERE
    p.post_type = "revision" AND
    p2.post_status = "publish"
    and p.post_name  NOT LIKE "%-autosave-%"
    AND u.ID IS NOT NULL
ORDER by p.post_date DESC
';
$revisions_by_author = $this->db->rawQuery($q);
$organised = array();
$totalcount = count($revisions_by_author);
echo $totalcount.' contributor stats to process'."\n";

foreach ($revisions_by_author as $r) {
    if (!isset($organised[$r['post_parent']])) {
        $organised[$r['post_parent']] = array();
    }
    if (!isset($organised[$r['post_parent']][$r['post_author']])) {
        $organised[$r['post_parent']][$r['post_author']] = 0;
    }
    $organised[$r['post_parent']][$r['post_author']]++;
}
// $this->db->rawQuery("TRUNCATE TABLE `dc_post_contributions`");
$finalcount = 0;
foreach ($organised as $post_id => $contr) {
    $contributors = count($contr);
    $x = 1;
    foreach ($contr as $u => $c) {
        //if ($x <= $maxcontributorsperpost) {
            $q = "INSERT INTO `_dc_post_contributions`
(`post_id`, `user_id`, `contr`)
VALUES ( '$post_id', '$u', '$c');";
            
            $this->db->rawQuery($q);
        //}
        $finalcount++;
        $x++;
    }
    
    $q = "INSERT INTO `_dc_post_contributions` 
(`post_id`, `user_id`, `contr`) 
VALUES ( '$post_id', '0', '$contributors');";
    $this->db->rawQuery($q);
    $finalcount++;
}
echo $finalcount.' contributor stats saved'."\n";

echo "Swapping tables\n";
$this->db->rawQuery("DROP TABLE `dc_post_contributions`");
$this->db->rawQuery("RENAME TABLE `_dc_post_contributions` TO `dc_post_contributions`;");


echo "Task complete\n";
