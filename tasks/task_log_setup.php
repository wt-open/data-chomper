<?php 
$tablename = "dc_task_log";
$q = "SELECT *
    FROM information_schema.tables
    WHERE table_schema = '".MYSQL_DB."'
        AND table_name = '$tablename'
    LIMIT 1;";

$r = $this->db->rawQuery($q);
if (empty($r)) {
    $r = $this->db->rawQuery(
    "CREATE TABLE `$tablename` (
      `log_id` bigint(20) NOT NULL,
      `task_name` varchar(50) NOT NULL,
      `start_datetime` datetime NOT NULL,
      `end_datetime` datetime NOT NULL,
      `output` text NOT NULL,
      `success` tinyint(1) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=".DB_CHARSET.";");
    $r = $this->db->rawQuery(
        "ALTER TABLE `$tablename`
      ADD PRIMARY KEY (`log_id`);");
    $r = $this->db->rawQuery(
        "ALTER TABLE `$tablename`
      MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT;");
    $r = $this->db->rawQuery(
        "COMMIT;");
}