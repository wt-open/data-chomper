<?php

require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);

// Using number of talk comments / most recent edits to order posts?

$tabs = array('all', 'published', 'drafts', 'projects');

$stories = $wptools->getAllPublishedAndDraftStoriesAndProjects(
		'ID, 
		post_type, 
		post_title, 
		post_modified, 
		post_status, 
		post_content, 
		post_type, 
		post_author', 
	0, 'post_modified DESC');

$options = $wptools->getOption('datachomped_homepagelist_data_pt');
if (empty($options)) {
	$ovars = array(
             'fresh_from_comment'    => true,
             'maxtostore'            => 0,
            'all'   => array (
                'mtitch'                => 0,
                'mchars'                => 0,
                'mcomme'                => 0,
                'medits'                => 0,
                'mcontr'                => 0,
                'mudays'                => 0,
                'minconditions'         => 0,
            ),
            'published'=> array (
                'mtitch'                => 0,
                'mchars'                => 0,
                'mcomme'                => 0,
                'medits'                => 0,
                'mcontr'                => 0,
                'mudays'                => 0,
                'minconditions'         => 0,
            ),
            'drafts'=> array (
                'mtitch'                => 0,
                'mchars'                => 0,
                'mcomme'                => 0,
                'medits'                => 0,
                'mcontr'                => 0,
                'mudays'                => 0,
                'minconditions'         => 0,
            ),
            'projects'=> array (
                'mtitch'                => 0,
                'mchars'                => 0,
                'mcomme'                => 0,
                'medits'                => 0,
                'mcontr'                => 0,
                'mudays'                => 0,
                'minconditions'         => 0,
               
            )
        );

    
} else {
    $ovars = unserialize($options);
    // echo 'DYNAMIC';
    
    
}
unset($options);

$list = [];
$c = 0;
// print_r($ovars);
// exit;


foreach ($stories as $story) {

	$stats = [];
	$stats['titch'] = strlen($story['post_title']);
	$txt = strip_tags($story['post_content']);
	$stats['chars'] = strlen($txt);

	$q = "SELECT count(comment_ID) as comment_count FROM `wp_comments` WHERE comment_post_ID = $story[ID] ";
	$r = $this->db->rawQuery($q);		
	$stats['comme'] = $r[0]['comment_count'];


	$q = "SELECT count(ID) as revision_count FROM `wp_posts` WHERE post_parent = $story[ID] ";
	$r = $this->db->rawQuery($q);
	$stats['edits'] = $r[0]['revision_count'];

	$q = "SELECT count(distinct(post_author)) as count_distinct_authors FROM `wp_posts` WHERE post_parent = $story[ID] ";
	$r = $this->db->rawQuery($q);
	$stats['contr'] = $r[0]['count_distinct_authors'];


	$q = "SELECT user_registered FROM `wp_users` WHERE ID = $story[post_author] ";
	$r = $this->db->rawQuery($q);
	$stats['udays'] = floor((time() - strtotime($r[0]['user_registered'])) / 60 / 60 / 24);

	// print_r($stats);

	// Quit this one if not good enough
	$points = [];
	foreach ($stats  as $v => $v) {
		$m = 'm'.$v;
		foreach ($tabs as $t) {
			if (empty($points[$t])) {
				$points[$t] = 0;
			}
			if ($stats[$v] >= $ovars[$t][$m]) {
				$points[$t]++;
			}
		}
	}
	// print_r($points);
	// die(max($points));
	// $t = array_sum($points);
	$q = false;
	$qualified = array();
	foreach ($tabs as $t) {
		// print_r($story);
		// exit;
		if ($points[$t] >= $ovars[$t]['minconditions']) {
			if (($t == 'published') && ($story['post_type'] == "stories") && ($story['post_status'] == 'publish')) {
				$q = true;
				$qualified[$t] = 1;
			} else if (($t == 'drafts') && ($story['post_type'] == "stories") && ($story['post_status'] == 'draft')) {
				$q = true;
				$qualified[$t] = 1;
			} else if (($t == 'projects') && ($story['post_type'] == "projects")) { 
				$q = true;
				$qualified[$t] = 1;
			} else  if ($t == 'all') {
				$q = true;
				$qualified[$t] = 1; 					
			} else {
				$qualified[$t] = 0; 					
			}

		} else {
			$qualified[$t] = 0; 	
		}

 	}
 	// if (($story['post_type'] == 'stories') && ($story['post_status'] == 'draft'))  {

 	// 	print_r($qualified);
 	// }
	// print_r($qualified);
 	/*if ($story['ID'] == 70054) {
 		if ($q == false) {
 			echo 'q is false';
 		} else {
 			echo 'q is true';
 		}
 		print_r($qualified);
 		print_r($points);
 		exit;
 	}*/
	if ($q == false) {
		continue;
	}

	// print_r($ovars);

	// print_r($points);
	// exit;
 // exit;
	 $q = "SELECT comment_date_gmt FROM `wp_comments` WHERE comment_post_ID = $story[ID] ORDER by comment_date_gmt DESC LIMIT 1 ";
	$r = $this->db->rawQuery($q);		
	$last_comment_date = (!empty($r[0]['comment_date_gmt'])) ? $r[0]['comment_date_gmt'] : 0;


		
	$used = '';
// print_r($ovars);
// exit;
	///   This is now 4 times
	if ($ovars['fresh_from_comment']) {
		if (
				(empty($last_comment_date)) 
					||
				($last_comment_date < $story['post_modified'])
			) {
			$usedate = $story['post_modified'];
			$used = 'last_comment_date-post_modified';
		} else {
			$usedate = $last_comment_date;
			$used = 'last_comment_date';		
		} 
	} else {
		$usedate = $story['post_modified']; 	
		$used = 'post_modified';
	}
// die();
	if (empty($points['projects'])) {
		// print_r($points);
		// exit;
	}
	$list[strtotime($usedate).'.'.$c] = [
		'post_id'				=>	$story['ID'], 
		'post_modified'			=>	$story['post_modified'],
		'post_status'			=>	$story['post_status'],
		'stats'					=>	$stats,
		'date_used'				=>	$used,
		'date_fresh'			=>	$usedate,
		'post_type'				=>	$story['post_type'],
		'qualify_all'			=>	$qualified['all'],
		'conditions_all'		=>	$points['all'],
		'qualify_published'		=>	$qualified['published'],
		'conditions_published'	=>	$points['published'],
		'qualify_drafts'		=>	$qualified['drafts'],
		'conditions_drafts'		=>	$points['drafts'],
		'qualify_projects'		=>	$qualified['projects'],
		'conditions_projects'	=>	$points['projects'],
	];

	$c++;
	// if ($vars['maxtostore'] > 0) {
	// 	if ($c >= $vars['maxtostore']) {
	// 		break;
	// 	}
	// }
		
}
// exit;
krsort($list);

echo count($list).' remain from '.count($stories)."\n";
 // print_r($list);
// exit;


echo "Writing temporary chart\n";
foreach ($list as $l) {

    $log_id = $this->db->insert('_dc_homepage_select_pertab', array(
        'post_id'       		=>  $l['post_id'],
        'titch'					=>  $l['stats']['titch'],
        'chars'					=>  $l['stats']['chars'],
        'comme'         		=>  $l['stats']['comme'],
        'edits'         		=>  $l['stats']['edits'],
        'contr'         		=>  $l['stats']['contr'],
        'udays'         		=>  $l['stats']['udays'],
        'post_type'				=>	$l['post_type'],
        'post_status'			=>	$l['post_status'],
        'usedate'      	 		=>  $l['date_used'],
        'date_fresh'			=>  $l['date_fresh'],
        'qualify_all'			=>  $l['qualify_all'],
        'conditions_all'		=>  $l['conditions_all'],
        'qualify_published'		=>  $l['qualify_published'],
        'conditions_published'	=>  $l['conditions_published'],
        'qualify_drafts'		=>  $l['qualify_drafts'],
        'conditions_drafts'		=>  $l['conditions_drafts'],
        'qualify_projects'		=>  $l['qualify_projects'],
        'conditions_projects'	=>  $l['conditions_projects'],
    ));
	if ($this->db->getLastErrno() === 0) {
	    // echo 'Update succesfull';
	
	} else {
	    echo 'Update failed. Error: '.$this->db->getLastError()."\n";
	}
}   



echo "Task complete\n";
