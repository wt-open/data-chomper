<?php

require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);

// Using number of talk comments / most recent edits to order posts?
$tablename = 'dc_homepage_select_algaerythm';


// print_r($optvars);
// exit;
// Some articles have 2 article states sometimes which messes things up
$q = 'SELECT meta_id, post_id FROM `wp_postmeta` WHERE meta_key = "article_state" group by post_id  having count(post_id) > 1';
$r = $this->db->rawQuery($q);		
foreach ($r as $p) {
	$q = 'delete from `wp_postmeta` where `meta_id` != "'.$p['meta_id'].'" AND `post_id` = "'.$p['post_id'].'" AND `meta_key` = "article_state";';

	$this->db->rawQuery($q);
}
// exit;
////
$stories = $wptools->getAllPublishedStoriesAndProjects(
		'ID, 
		post_type, 
		post_title, 
		post_date, 
		post_modified, 
		post_date_gmt, 
		post_modified_gmt, 
		post_status, 
		post_content, 
		post_type, 
		post_author', 
	0, 'post_modified DESC');


$list = [];
$c = 0;
// print_r($ovars);
// exit;
$rc = count($stories) + 1;



$this->WTA->setOptVars('algaerythm');

foreach ($stories as $story) {

	$latest = $wptools->getLastRevisionForPost($story['ID']);
	$accepted = $wptools->getAcceptedRevisionForPost($story['ID']);

	if (!empty($accepted)) {
		$idtouse = $accepted['ID'];
	} else if (!empty($latest)) { 
		$idtouse = $latest['ID'];
	} else {
		$idtouse = $story['ID'];
	}

	$article_state = $wptools->getPostMeta($idtouse, 'article_state');

	// echo '#'.$article_state.'<br>';

	$list[] = $this->WTA->run('algaerythm', [
		'story'			=>	$story, 
		'latest'		=>	$latest, 
		'accepted'		=> 	$accepted,
		'article_state'	=>	$article_state
	]);
		
}

// krsort($list);

echo count($list).' remain from '.count($stories)."\n";
 // print_r($list);
// exit;


echo "Writing temporary chart\n";
foreach ($list as $l) {
    $log_id = $this->db->insert('_'.$tablename, array(
        	'post_id' 			=> $l['post_id'],
			'newtab_date'		=> $l['newtab_date'],
			'talktab_date'		=> $l['talktab_date'],
			'worktab_date'		=> $l['worktab_date'],
			'speed_comments'	=> $l['speed_comments'],
			'edit_speed'		=> $l['edit_speed'],
			'attention'			=> implode(', ', $l['attention']),
    ));
	if ($this->db->getLastErrno() === 0) {
	    // echo 'Update succesfull';
	


	} else {
	    echo 'Update failed. Error: '.$this->db->getLastError()."\n";
	}
}   
  

echo "Task complete\n";
