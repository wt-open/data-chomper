<?php 
if (!empty($tablename)) {
    $q[] = 
    "CREATE TABLE `$tablename` (
          `hp_id` bigint(20) NOT NULL,
          `post_id` bigint(20) NOT NULL,
          `newtab_date` datetime NOT NULL,
          `talktab_date` datetime NOT NULL,
          `worktab_date` datetime NOT NULL,
          `speed_comments` int(11) NOT NULL,
          `edit_speed` int(11) NOT NULL,
          `attention` varchar(255) NOT NULL

        ) ENGINE=InnoDB DEFAULT CHARSET=".DB_CHARSET.";";
    
    $q[] = "ALTER TABLE `$tablename`
        ADD PRIMARY KEY (`hp_id`);";
    
    $q[] = "ALTER TABLE `$tablename` ADD INDEX(`post_id`);";
    $q[] = "ALTER TABLE `$tablename` ADD FULLTEXT(`attention`);";
    
    
    $q[] = "ALTER TABLE `$tablename`
        MODIFY `hp_id` bigint(20) NOT NULL AUTO_INCREMENT;";
   $q[] = "COMMIT;";
        
}

?>
