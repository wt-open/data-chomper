<?php

require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);

// Using number of talk comments / most recent edits to order posts?

$tabs = array('all', 'published', 'drafts', 'projects');


$stories = $wptools->getAllPublishedStoriesAndProjects(
		'ID, 
		post_type, 
		post_title, 
		post_date, 
		post_modified, 
		post_status, 
		post_content, 
		post_type, 
		post_author', 
	0, 'post_modified DESC');

$options = $wptools->getOption('datachomped_homepagelist_data_pt');
if (empty($options)) {
	$ovars = array(
             // 'fresh_from_comment'    => true,
             'maxtostore'            => 0,
            'all'   => array (
                'mtitch'                => 0,
                'mchars'                => 0,
                'mcomme'                => 0,
                'medits'                => 0,
                'mcontr'                => 0,
                'mudays'                => 0,
                'minconditions'         => 0,
             	'fresh_from_comment'    => 1,
            ),
            'published'=> array (
                'mtitch'                => 0,
                'mchars'                => 0,
                'mcomme'                => 0,
                'medits'                => 0,
                'mcontr'                => 0,
                'mudays'                => 0,
                'minconditions'         => 0,
             	'fresh_from_comment'    => 1,
            ),
            'drafts'=> array (
                'mtitch'                => 0,
                'mchars'                => 0,
                'mcomme'                => 0,
                'medits'                => 0,
                'mcontr'                => 0,
                'mudays'                => 0,
                'minconditions'         => 0,
             	'fresh_from_comment'    => 1,
            ),
            'projects'=> array (
                'mtitch'                => 0,
                'mchars'                => 0,
                'mcomme'                => 0,
                'medits'                => 0,
                'mcontr'                => 0,
                'mudays'                => 0,
                'minconditions'         => 0,
             	'fresh_from_comment'    => 1,
            )
        );

    
} else {
    $ovars = unserialize($options);
    // echo 'DYNAMIC';
    
    
}
unset($options);

$list = [];
$c = 0;
// print_r($ovars);
// exit;


// Ordering vars

$weights = unserialize($wptools->getOption('datachomped_homepagelist_data_rank_weight'));
$affects = unserialize($wptools->getOption('datachomped_homepagelist_data_rank_affect'));
// print_r($weights);
// exit;
$factoryorderingvars = [
	'created'=> [
		'affect'=>'negative',
		'weight'=>'high',
	],
	'last_update'=> [
		'affect'=>'negative',
		'weight'=>'medium',
	], 
	'no_comments'=> [
		'affect'=>'positive',
		'weight'=>'tenth',
	],
	'no_editors'=> [
		'affect'=>'positive',
		'weight'=>'low',
	], 
	'no_edits'=> [
		'affect'=>'positive',
		'weight'=>'low',
	], 
	'no_daysuser'=> [
		'affect'=>'positive',
		'weight'=>'tenth',
	], 
	'no_users_read'=> [
		'affect'=>'positive',
		'weight'=>'low',
	],

];

foreach ($tabs as $t) {
    $orderingvars[$t] = $factoryorderingvars;
    
    if (isset($affects[$t])) {
        foreach ($affects[$t] as $k => $a) {
            $orderingvars[$t][$k]['affect'] = $a;
        }
    }
    if (isset($weights[$t])) {
        foreach ($weights[$t] as $k => $w) {
            $orderingvars[$t][$k]['weight'] = $w;
        }
    }
}
// print_r($orderingvars);
// exit;

foreach ($stories as $story) {



	$latest = $wptools->getLastRevisionForPost($story['ID']);
	if (!empty($latest)) {
		$article_state = $wptools->getPostMeta($latest['ID'], 'article_state');
	}

	if (empty($article_state)) {
		if ($story['post_type'] == 'projects') {
			$article_state = 'publish';
		} else {
			$article_state = 'draft';
		}
	}
	// if ($story['ID'] == 77132) {
	// 	echo $article_state.'<Br>';
	// 	print_r($story);
	// 	exit;
	// }
	$stats = [];
	$stats['titch'] = strlen($story['post_title']);
	$txt = strip_tags($story['post_content']);
	$stats['chars'] = strlen($txt);

	$q = "SELECT count(comment_ID) as comment_count FROM `wp_comments` WHERE comment_post_ID = $story[ID] ";
	$r = $this->db->rawQuery($q);		
	$stats['comme'] = $r[0]['comment_count'];


	$q = "SELECT count(ID) as revision_count FROM `wp_posts` WHERE post_parent = $story[ID] ";
	$r = $this->db->rawQuery($q);
	$stats['edits'] = $r[0]['revision_count'];

	$q = "SELECT count(distinct(post_author)) as count_distinct_authors FROM `wp_posts` WHERE post_parent = $story[ID] ";
	$r = $this->db->rawQuery($q);
	$stats['contr'] = $r[0]['count_distinct_authors'];


	$q = "SELECT user_registered FROM `wp_users` WHERE ID = $story[post_author] ";
	$r = $this->db->rawQuery($q);
	$stats['udays'] = floor((time() - strtotime($r[0]['user_registered'])) / 60 / 60 / 24);

	$q = "SELECT count(DISTINCT(user_id)) as count_distinct_readers FROM `dc_post_read_log` WHERE post_id = $story[ID]";
	$r = $this->db->rawQuery($q);
	$total_users_read = $r[0]['count_distinct_readers']; 
	// echo $story['ID'].' = '.$total_users_read.'<br>';
	// print_r($stats);

	// Quit this one if not good enough
	$points = [];
	foreach ($stats  as $v => $v) {
		$m = 'm'.$v;
		foreach ($tabs as $t) {
			if (empty($points[$t])) {
				$points[$t] = 0;
			}
			if ($stats[$v] >= $ovars[$t][$m]) {
				$points[$t]++;
			}
		}
	}
	// print_r($points);
	// die(max($points));
	// $t = array_sum($points);
	$qc = false;
	$qualified = array();
	$last_comment_date = [];
	$used = [];


	 $q = "SELECT comment_date_gmt FROM `wp_comments` WHERE comment_post_ID = $story[ID] ORDER by comment_date_gmt DESC LIMIT 1 ";
	$r = $this->db->rawQuery($q);		
	$last_comment_date = (!empty($r[0]['comment_date_gmt'])) ? $r[0]['comment_date_gmt'] : 0;

	$la = [];
	foreach ($tabs as $t) {
		$la[$t] = '-unset-';
	}

	$used = $la;
	$usedate = $la;

	foreach ($tabs as $t) {
		// print_r($story);
		// exit;
		if ($points[$t] >= $ovars[$t]['minconditions']) {
			if (($t == 'published') && ($story['post_type'] == "stories") && ($article_state == 'publish')) {
				$qc = true;
				$qualified[$t] = 1;
			} else if (($t == 'drafts') && ($story['post_type'] == "stories") && ($article_state == 'draft')) {
				$qc = true;
				$qualified[$t] = 1;
			} else if (($t == 'projects') && ($story['post_type'] == "projects")) { 
				$qc = true;
				$qualified[$t] = 1;
			} else  if ($t == 'all') {
				$qc = true;
				$qualified[$t] = 1; 					
			} else {
				$qualified[$t] = 0; 					
			}

		} else {
			$qualified[$t] = 0; 	
		}


	// print_r($ovars);
	// exit;
		

		///   This is now 4 times
		if (!isset($ovars[$t]['fresh_from_comment'])) {
			// echo $t.'<br>';
			// print_r($ovars[$t]);
			// exit;
			$ovars[$t]['fresh_from_comment'] = 0;
		}
		if ($ovars[$t]['fresh_from_comment']) {
			if (
					(empty($last_comment_date)) 
						||
					($last_comment_date < $story['post_modified'])
				) {
				$usedate[$t] = $story['post_modified'];
				$used[$t] = 'last_comment_date-post_modified';
			} else {
				$usedate[$t] = $last_comment_date;
				$used[$t] = 'last_comment_date';		
			} 
		} else {
			$usedate[$t] = $story['post_modified']; 	
			$used[$t] = 'post_modified';
		}

 	}
 	// if (($story['post_type'] == 'stories') && ($story['post_status'] == 'draft'))  {

 	// 	print_r($qualified);
 	// }
	// print_r($qualified);
 	/*if ($story['ID'] == 70054) {
 		if ($q == false) {
 			echo 'q is false';
 		} else {
 			echo 'q is true';
 		}
 		print_r($qualified);
 		print_r($points);
 		exit;
 	}*/
	if ($qc == false) {
		continue;
	}

	// print_r($ovars);

	// print_r($points);
	// exit;
 // exit;
	

	// Now we work out the ordering factor
	// print_r($last_comment_date);
	// exit;

	// echo $story['ID'].' ... ';

	$order_points = [];
	foreach ($tabs as $t) {
		// echo $t.'<Br>';
		$factors = [
			'created'				=> (((time() - strtotime($story['post_date'])) / 60) / 24) ,
			'last_update'			=> (((time() - strtotime($usedate[$t])) / 60) / 24) ,
			'no_comments'			=> $stats['comme'],
			'no_editors'			=> $stats['contr'], 
			'no_edits'				=> $stats['edits'], 
			'no_daysuser'			=> $stats['udays'] ,
			'no_users_read'			=> $total_users_read
		];
		$p = 0;
		foreach ($orderingvars[$t] as $k => $v) {
			$n = 0;
			if ($v['weight'] == 'low') {
				$n = $factors[$k];
			} else if ($v['weight'] == 'medium') {
				$n = $factors[$k] * 2;
			} else if ($v['weight'] == 'high') {
				$n = $factors[$k] * 4;
			} else if ($v['weight'] == 'tenth') {
				$n = $factors[$k] / 10;
			}


			if ($v['affect'] == 'negative') {
				$p = $p - $n;
			} else if ($v['affect'] == 'positive') {
				$p = $p + $n;
			}
			// echo $k.' '.$n.' | ';

		}
		// WE multiply by 100 so that we the integer has something to work with
		$order_points[$t] = round($p * 100);
	}
	// echo '<pre>'.print_r($order_points).'</pre>';
	// exit;



	$list[$story['ID']] = [
		'tab'					=>	$t,
		'post_id'				=>	$story['ID'], 
		'post_modified'			=>	$story['post_modified'],
		'post_status'			=>	$article_state,
		'stats'					=>	$stats,
		'post_type'				=>	$story['post_type'],

		'qualify_all'			=>	$qualified['all'],
		'conditions_all'		=>	$points['all'],
		'qualify_published'		=>	$qualified['published'],
		'conditions_published'	=>	$points['published'],
		'qualify_drafts'		=>	$qualified['drafts'],
		'conditions_drafts'		=>	$points['drafts'],
		'qualify_projects'		=>	$qualified['projects'],
		'conditions_projects'	=>	$points['projects'],

		'order_points'			=>	$order_points
	];

	$c++;
	// }
	// if ($vars['maxtostore'] > 0) {
	// 	if ($c >= $vars['maxtostore']) {
	// 		break;
	// 	}
	// }
		
}
// exit;
// krsort($list);

echo count($list).' remain from '.count($stories)."\n";
 // print_r($list);
// exit;


echo "Writing temporary chart\n";
foreach ($list as $l) {

    $log_id = $this->db->insert('_dc_homepage_select_reorder ', array(
        'post_id'       		=>  $l['post_id'],
        'titch'					=>  $l['stats']['titch'],
        'chars'					=>  $l['stats']['chars'],
        'comme'         		=>  $l['stats']['comme'],
        'edits'         		=>  $l['stats']['edits'],
        'contr'         		=>  $l['stats']['contr'],
        'udays'         		=>  $l['stats']['udays'],
        'post_type'				=>	$l['post_type'],
        'post_status'			=>	$l['post_status'],
        
        'order_all'      	 	=>  $l['order_points']['all'],
        'order_published'      	=>  $l['order_points']['published'],
        'order_drafts'      	=>  $l['order_points']['drafts'],
        'order_projects'      	=>  $l['order_points']['projects'],
       
        'qualify_all'			=>  $l['qualify_all'],
        'conditions_all'		=>  $l['conditions_all'],
        'qualify_published'		=>  $l['qualify_published'],
        'conditions_published'	=>  $l['conditions_published'],
        'qualify_drafts'		=>  $l['qualify_drafts'],
        'conditions_drafts'		=>  $l['conditions_drafts'],
        'qualify_projects'		=>  $l['qualify_projects'],
        'conditions_projects'	=>  $l['conditions_projects'],
    ));
	if ($this->db->getLastErrno() === 0) {
	    // echo 'Update succesfull';
	
	} else {
	    echo 'Update failed. Error: '.$this->db->getLastError()."\n";
	}
}   



echo "Task complete\n";
