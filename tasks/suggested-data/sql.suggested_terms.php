<?php 
if (!empty($tablename)) {
    $q[] = "
    CREATE TABLE `$tablename` (
        `sug_id` bigint(20) NOT NULL,
        `term_id` bigint(20) NOT NULL,
        `post_id` bigint(20) NOT NULL,
        `count` int(11) NOT NULL
        )ENGINE=InnoDB DEFAULT CHARSET=".DB_CHARSET.";";
    
    $q[] = "ALTER TABLE `$tablename`
  ADD PRIMARY KEY (`sug_id`),
  ADD KEY `sug_term_id` (`term_id`),
  ADD KEY `sug_post_id` (`post_id`);";
    
    
    $q[] = "ALTER TABLE `$tablename`
        MODIFY `sug_id` bigint(20) NOT NULL AUTO_INCREMENT;";
   $q[] = "COMMIT;";
        
}

?>
