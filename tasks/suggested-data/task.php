<?php
require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);


$taxonomy = $wptools->getAllTaxonomy();

// $post_ids = $this->get_job_list_post_ids();

/* For the purposes of testing */
$post_ids  = $wptools->getAllPublishedStories(array('ID'), 0);
foreach ($post_ids as $k => $p) {
    $post_ids[$k] = $p['ID'];
}
/* END TEST */


$found = array();
foreach ($post_ids as $post_id) {
    $wptools->setPostId($post_id);
    $found[$post_id] = array();
    $tosearch = array(
        $wptools->getPostContent()
    );
    
    $prim = $wptools->getStoryPrimaryCategory(true);
    $ignore = array_merge(
        array('term_id'=>reset($prim)),
        $wptools->getPostCategories(true),
        $wptools->getPostTags(true)
    );
    foreach ($ignore as $k => $t) {
        $ignore[$k] = $t['term_id'];
    }
    foreach ($tosearch as $s) {
        foreach ($taxonomy as $t) {
            if (!in_array($t['term_id'], $ignore)) {
                $sc = substr_count($s, $t['name']);
                if ($sc  > 0) {
                    $found[$post_id][$t['term_id']] = $sc;
                }
            }
        }
        arsort($found[$post_id]);
    }
}
$x = 1;
$threshold = 2;
$suggestions = 0;
foreach ($found as $post_id => $list) {
    $wptools->setPostId($post_id);
    //echo '---'.$x.' / '.$post_id.' / '.$wptools->getPostTitle().'---'."\n";
    foreach ($list as $term_id => $count) {
        if ($count < $threshold) {
            continue;
        }
        //echo $taxonomy[$term_id]['name']." / #".$term_id." / +".$count."\n";
        
        $q = 'INSERT into _dc_suggested_terms
(term_id, post_id, count)
VALUES ('.$term_id.', '.$post_id.', '.$count.')';
        $this->db->rawQuery($q);
        $suggestions++;
    }
    $x++;
    
}
echo $suggestions .' suggestions saved'."\n";
echo "Task complete\n";
