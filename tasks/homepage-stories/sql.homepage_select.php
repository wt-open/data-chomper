<?php 
if (!empty($tablename)) {
    $q[] = 
    "CREATE TABLE `$tablename` (
        `stat_id` bigint(20) NOT NULL,
        `post_id` bigint(20) NOT NULL,
        `post_type` varchar(20) NOT NULL,
        `post_status` varchar(20) NOT NULL,
        `chars` int(11) NOT NULL,
        `comme` int(20) NOT NULL,
        `edits` int(11) NOT NULL,
        `contr` int(20) NOT NULL,
        `udays` int(20) NOT NULL,
        `conditions` int(3) NOT NULL,
        `usedate` varchar(20) NOT NULL,
        `date_fresh` datetime NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=".DB_CHARSET.";";
    
    $q[] = "ALTER TABLE `$tablename`
        ADD PRIMARY KEY (`stat_id`);";
    
    $q[] = "ALTER TABLE `$tablename` ADD INDEX(`post_type`);";
    
    
    $q[] = "ALTER TABLE `$tablename`
        MODIFY `stat_id` bigint(20) NOT NULL AUTO_INCREMENT;";
   $q[] = "COMMIT;";
        
}

?>
