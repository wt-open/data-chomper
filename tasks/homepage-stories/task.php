<?php

require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);

// Using number of talk comments / most recent edits to order posts?



$stories = $wptools->getAllPublishedAndDraftStoriesAndProjects(
		'ID, 
		post_type, 
		post_title, 
		post_modified, 
		post_status, 
		post_content, 
		post_type, 
		post_author', 
	0, 'post_modified DESC');

$options = $wptools->getOption('datachomped_homepagelist_data');
if (empty($options)) {
    $vars = array (
        'maxtostore'     		=>  0,
        'mchars'    			=>  100,
        'mcomme'    			=>  3,
        'medits'        		=>  2,
        'mcontr'              	=>  2,
        'mudays'        		=>  1,
        'minconditions'			=>  3,
        'fresh_from_comment'    =>  true,
    );
    
} else {
    $options = unserialize($options);
    // echo 'DYNAMIC';
    $vars = array (
    	'maxtostore'     		=>  $options['maxtostore'],
        'mchars'    			=>  $options['mchars'],
        'mcomme'    			=>  $options['mchars'],
        'medits'        		=>  $options['medits'],
        'mcontr'              	=>  $options['mcontr'],
        'mudays'        		=>  $options['mudays'],
        'minconditions'			=>  $options['minconditions'],
        'fresh_from_comment'    =>  $options['fresh_from_comment']
    );
    
}
unset($options);

// print_r($vars);
// exit;
$list = [];
$c = 0;
foreach ($stories as $story) {
	$d = 0;	
	$stats = [];
	$txt = strip_tags($story['post_content']);
	$stats['chars'] = strlen($txt);
	if ($stats['chars'] >= $vars['mchars']) {
		$d++;
	}

	$q = "SELECT count(comment_ID) as comment_count FROM `wp_comments` WHERE comment_post_ID = $story[ID] ";
	$r = $this->db->rawQuery($q);		
	$stats['comme'] = $r[0]['comment_count'];
	if ($stats['comme'] >= $vars['mcomme']) {
		$d++;
	}

	$q = "SELECT count(ID) as revision_count FROM `wp_posts` WHERE post_parent = $story[ID] ";
	$r = $this->db->rawQuery($q);
	$stats['edits'] = $r[0]['revision_count'];
	if ($stats['edits'] >= $vars['medits']) {
		$d++;
	}

	$q = "SELECT count(distinct(post_author)) as count_distinct_authors FROM `wp_posts` WHERE post_parent = $story[ID] ";
	$r = $this->db->rawQuery($q);
	$stats['contr'] = $r[0]['count_distinct_authors'];
	if ($stats['contr'] >= $vars['mcontr']) {
	 	$d++;
	}


	$q = "SELECT user_registered FROM `wp_users` WHERE ID = $story[post_author] ";
	$r = $this->db->rawQuery($q);
	$stats['udays'] = floor((time() - strtotime($r[0]['user_registered'])) / 60 / 60 / 24);
	if ($stats['udays'] >= $vars['mudays']) {
	 	$d++;
	}


	// Quit this one if not good enough
	if ($d < $vars['minconditions']) {
		continue;
	}


	 $q = "SELECT comment_date_gmt FROM `wp_comments` WHERE comment_post_ID = $story[ID] ORDER by comment_date_gmt DESC LIMIT 1 ";
	$r = $this->db->rawQuery($q);		
	$last_comment_date = (!empty($r[0]['comment_date_gmt'])) ? $r[0]['comment_date_gmt'] : 0;


	
	$used = '';
	if ($vars['fresh_from_comment']) {
		if (
				(empty($last_comment_date)) 
					||
				($last_comment_date < $story['post_modified'])
			) {
			$usedate = $story['post_modified'];
			$used = 'last_comment_date-post_modified';
		} else {
			$usedate = $last_comment_date;
			$used = 'last_comment_date';		
		} 
	} else {
		$usedate = $story['post_modified']; 	
		$used = 'post_modified';
	}


	$list[strtotime($usedate).'.'.$c] = [
		'post_id'		=>	$story['ID'], 
		'post_modified'	=>	$story['post_modified'],
		'post_status'	=>	$story['post_status'],
		'stats'			=>	$stats,
		'date_used'		=>	$used,
		'date_fresh'	=>	$usedate,
		'post_type'		=>	$story['post_type'],
		'conditions'	=>	$d
	];

	$c++;
	if ($vars['maxtostore'] > 0) {
		if ($c >= $vars['maxtostore']) {
			break;
		}
	}
	// exit;	
}
krsort($list);

echo count($list).' remain from '.count($stories)."\n";
// print_r($list);



echo "Writing temporary chart\n";
foreach ($list as $l) {

    $log_id = $this->db->insert('_dc_homepage_select', array(
        'post_id'         =>  $l['post_id'],
        'chars'			  =>  $l['stats']['chars'],
        'comme'         =>  $l['stats']['comme'],
        'edits'         =>  $l['stats']['edits'],
        'contr'         =>  $l['stats']['contr'],
        'udays'         =>  $l['stats']['udays'],
        'post_type'		=>	$l['post_type'],
        'post_status'		=>	$l['post_status'],
        'usedate'         =>  $l['date_used'],
        'date_fresh'	=> $l['date_fresh'],
        'conditions'	=> $l['conditions'],
    ));

}   



echo "Task complete\n";
