<?php

require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);


$options = $wptools->getOption('datachomped_relatedstories_data');
if (empty($options)) {
    $weights = array (
        'primary_topic'     =>  25,
        'sub_is_primary'    =>  20,
        'categories'        =>  10,
        'tags'              =>  6,
        'story_type'        =>  3,
    );
    $num_stories = 6;
} else {
    $options = unserialize($options);
    
    $weights = array (
        'primary_topic'     =>  $options['primary_topic'],
        'sub_is_primary'    =>  $options['sub_is_primary'],
        'categories'        =>  $options['categories'],
        'tags'              =>  $options['tags'],
        'story_type'        =>  $options['story_type'],
    );
    $num_stories = $options['num_stories'];
    
}
unset($options);

/*
 * TODO: If the titles are similar - points
 * TODO: If the story types are the same - points - on second thoughts - no
 */


function createRSTable($dbo, $preload = false, $onlyifnotexists = true) {
    $tablename = (!$preload) ? 'dc_related_stories' : '_pl_dc_related_stories';
    
    if ($onlyifnotexists) {
            $q = "SELECT *
        FROM information_schema.tables
        WHERE table_schema = 'wikitribune'
            AND table_name = '$tablename'
        LIMIT 1;";
        
       $r = $dbo->rawQuery($q);
       if (!empty($r)) {
           return false;
       }
        
    }
    $cq = array(
        "CREATE TABLE `$tablename` (
            `rel_id` bigint(20) NOT NULL,
            `post_id_1` bigint(20) NOT NULL,
            `post_id_2` bigint(20) NOT NULL,
            `points` int(9) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=".DB_CHARSET.";",
        "ALTER TABLE `$tablename`
              ADD PRIMARY KEY (`rel_id`),
              ADD KEY `rel_id` (`rel_id`),
              ADD KEY `post_id_1` (`post_id_1`),
              ADD KEY `post_id_2` (`post_id_2`);",
        "ALTER TABLE `$tablename`
          MODIFY `rel_id` bigint(20) NOT NULL AUTO_INCREMENT;",
        "COMMIT;"
        
    );
    foreach ($cq as $q) {
        $dbo->rawQuery($q);        
    }
}


createRSTable($this->db);
createRSTable($this->db, true);

$statistics = array();



// Primary Category Stuff -----------------------------------------


$cols = 'post_id, post_title, t.term_id, parent, name, slug, count';
// $cols = '*';
$q = 'SELECT  '.$cols.'
FROM `wp_postmeta` pm
LEFT JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id  = pm.meta_value
LEFT JOIN wp_terms t ON t.term_id = tt.term_id
LEFT JOIN wp_posts p on pm.post_id = p.ID

WHERE meta_key =  "primary_topic"
    AND p.post_type = "stories"  
    AND p.post_status = "publish"
';
$post_meta_category_story_associations = $this->db->rawQuery($q);
echo count($post_meta_category_story_associations)." post_meta category story associations\n\n";

$primary_category_lookup = array();

foreach ($post_meta_category_story_associations as $p) {
    $primary_category_lookup[$p['term_id']][] = $p['post_id'];
}
$points = $weights['primary_topic'];

$posts_by_category = array();

foreach ($primary_category_lookup as $term_id => $post_ids) {
    
    if ($term_id > 0) {
        foreach ($post_ids as $p1) {
            if (empty($posts_by_category[$term_id])) {
                $posts_by_category[$term_id] = array();
            }
            $posts_by_category[$term_id][] = $p1;
            
            foreach ($post_ids as $p2) {
                if ($p1 != $p2) {
                    if (!isset($statistics[$p1][$p2])) {
                        $statistics[$p1][$p2] = (isset($statistics[$p1][$p2])) ? $statistics[$p1][$p2] + $points : $points;
                    }
                }
            }
        }
    }
}


// Sub Catgories and Tags Category Stuff -----------------------------------------

$cols = 'object_id, t.term_id, taxonomy, term_order, slug ';
// $cols = '*';
$q = 'SELECT '.$cols.'
FROM wp_term_relationships r
LEFT JOIN wp_term_taxonomy tt on r.term_taxonomy_id = tt.term_taxonomy_id
LEFT JOIN wp_terms t ON t.term_id = tt.term_id
LEFT JOIN wp_posts p on r.object_id = p.ID
WHERE taxonomy = ( "category" OR taxonomy = "tag" )
    AND p.post_type = "stories"
    AND p.post_status = "publish"
ORDER by term_order
';
$categories_and_tag_story_associations = $this->db->rawQuery($q);
echo count($categories_and_tag_story_associations)." categories and tag story associations\n\n";


$category_lookup = array();
foreach ($categories_and_tag_story_associations as $p) {
    
    if ($p['taxonomy'] == 'category') {
        $category_lookup['categories'][$p['term_id']][] = $p['object_id'];
    } else if ($p['taxonomy'] == 'post_tag') {
        $category_lookup['tags'][$p['term_id']][] = $p['object_id'];
    }
}
foreach($category_lookup as $tt => $data) {
    if ($tt == 'categories') {
        $points = $weights['categories'];
    } else if ($tt == 'tags') {
        $points = $weights['tags'];
    }
    foreach ($data as $term_id => $post_ids) {
        foreach ($post_ids as $p1) {
            if (isset($posts_by_category[$term_id])) {
                foreach ($posts_by_category[$term_id] as $p2) {
                    $statistics[$p1][$p2] = (isset($statistics[$p1][$p2])) ? $statistics[$p1][$p2] + $weights['sub_is_primary'] : $weights['sub_is_primary'];
                }
            }
            foreach ($post_ids as $p2) {
                if ($p1 != $p2) {
                    $statistics[$p1][$p2] = (isset($statistics[$p1][$p2])) ? $statistics[$p1][$p2] + $points : $points;
                }
            }
        }
    }
}




// Story Type Stuff -----------------------------------------

/*
$cols = 'post_id, meta_value';
// $cols = '*';
$q = 'SELECT  '.$cols.'
FROM `wp_postmeta` pm
LEFT JOIN wp_posts p on pm.post_id = p.ID
WHERE meta_key =  "story_type"
    AND p.post_type = "stories"
    AND p.post_status = "publish"
';
$post_meta_story_types_associations = $this->db->rawQuery($q);
echo count($post_meta_story_types_associations)." post_meta story type associations\n\n";

$storytype_lookup = array();

foreach ($post_meta_story_types_associations as $p) {
    $storytype_lookup[$p['meta_value']][] = $p['post_id'];
}
$points = $weights['story_type'];
foreach ($storytype_lookup as $meta_value => $post_ids) {
    if ($meta_value > "") {
        foreach ($post_ids as $p1) {
            foreach ($post_ids as $p2) {
                if ($p1 != $p2) {
                    if (!isset($statistics[$p1][$p2])) {
                        $statistics[$p1][$p2] = (isset($statistics[$p1][$p2])) ? $statistics[$p1][$p2] + $points : $points;
                    }
                }
            }
        }
    }
}

print_r($statistics);
exit;
*/




// Empty the table ready for rejig


//$this->db->rawQuery("TRUNCATE TABLE `dc_related_stories`");
$totallinks = 0;
foreach ($statistics as $k1 => $d1) {
    arsort($statistics[$k1]);
    $statistics[$k1] =  array_slice($statistics[$k1], 0, $num_stories, true);
    foreach ($statistics[$k1] as $k2 => $points) {
        $q = 'INSERT into _pl_dc_related_stories (post_id_1, post_id_2, points) VALUES ('.$k1.', '.$k2.', '.$points.')';
        $this->db->rawQuery($q);
    }
    $totallinks = $totallinks + count($statistics[$k1]);
}
echo "Swapping tables\n";
$this->db->rawQuery("DROP TABLE `dc_related_stories`");
$this->db->rawQuery("RENAME TABLE `_pl_dc_related_stories` TO `dc_related_stories`;");
echo $totallinks." links created\n\n";
//print_r($statistics);]
echo "Task complete\n";

