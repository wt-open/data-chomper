<?php 
if (!empty($tablename)) {
    $q[] = 
    "CREATE TABLE `$tablename` (
        `stat_id` bigint(20) NOT NULL,
        `term_id` bigint(20) NOT NULL,
        `post_type` VARCHAR(255) NOT NULL,
        `primary_topic` bigint(20) NOT NULL,
        `categories` bigint(20) NOT NULL,
        `tags` bigint(20) NOT NULL,
        `total` bigint(20) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=".DB_CHARSET.";";
    
    $q[] = "ALTER TABLE `$tablename`
        ADD PRIMARY KEY (`stat_id`);";
    
    
    $q[] = "ALTER TABLE `$tablename`
        MODIFY `stat_id` bigint(20) NOT NULL AUTO_INCREMENT;";
   $q[] = "COMMIT;";
        
}

?>
