    <?php
require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);

$published_stories = $wptools->getAllPublishedStories('ID, post_type', 0);
$published_projects = $wptools->getAllPublishedProjects('ID, post_type', 0);
$smushed = array_merge($published_stories, $published_projects);


$terms = $wptools->getAllTaxonomy(array('name','slug'), 0);

$stats = array();
foreach($smushed  as $s) {
    $wptools->setPostId($s['ID']);
    
    $primary = $wptools->getStoryPrimaryCategory(true); 
    $cats = $wptools->getPostCategories(true); 
    $tags = $wptools->getPostTags(true); 
    
    $primary = reset($primary);
    if (!empty($primary['term_id'])) {
        if (empty($stats[$s['post_type']][$primary['term_id']]['primary_topic'])) {
            $stats[$s['post_type']][$primary['term_id']]['primary_topic'] = 1;
        } else {
            $stats[$s['post_type']][$primary['term_id']]['primary_topic']++;
        }
    }
    if (!empty($cats)) {
        foreach ($cats as $c) {
            if (empty($stats[$s['post_type']][$c['term_id']]['categories'])) {
                $stats[$s['post_type']][$c['term_id']]['categories'] = 1;
            } else {
                $stats[$s['post_type']][$c['term_id']]['categories']++;
            }
        }
    }
    if (!empty($tags)) {
        foreach ($tags as $t) {
            if (empty($stats[$s['post_type']][$t['term_id']]['tags'])) {
                $stats[$s['post_type']][$t['term_id']]['tags'] = 1;
            } else {
                $stats[$s['post_type']][$t['term_id']]['tags']++;
            }
        }
    }
}
foreach ($stats as $post_type => $sts) {
    foreach ($sts as $term_id => $st) {
        $p = (isset($st['primary_topic'])) ? $st['primary_topic'] : 0;
        $c = (isset($st['categories'])) ? $st['categories'] : 0;
        $t = (isset($st['tags'])) ? $st['tags'] : 0;
        $tt = $p + $c + $t;
        
        $q = 'INSERT into _dc_taxonomy_stats 
    (term_id, post_type, primary_topic, categories, tags, total) 
    VALUES ('.$term_id.', "'.$post_type.'", '.$p.', '.$c.', '.$t.', '.$tt.')';
        $this->db->rawQuery($q);
    }
}


echo "Task complete\n";