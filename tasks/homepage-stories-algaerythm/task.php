<?php

require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);

// Using number of talk comments / most recent edits to order posts?
$tablename = 'dc_homepage_select_algaerythm';

$options = [
	'speedper' 			=> $wptools->getOption('datachomped_homepagelist_algaerythm_speedtime')


];
$optvars = [
	'date_for_speed'				=> date('Y-m-d H:i:s', time() - $options['speedper']),
	'min_length_of_content_for_new'	=> 50  
];
// print_r($optvars);
// exit;
// Some articles have 2 article states sometimes which messes things up
$q = 'SELECT meta_id, post_id FROM `wp_postmeta` WHERE meta_key = "article_state" group by post_id  having count(post_id) > 1';
$r = $this->db->rawQuery($q);		
foreach ($r as $p) {
	$q = 'delete from `wp_postmeta` where `meta_id` != "'.$p['meta_id'].'" AND `post_id` = "'.$p['post_id'].'" AND `meta_key` = "article_state";';

	$this->db->rawQuery($q);
}
// exit;
////
$stories = $wptools->getAllPublishedStoriesAndProjects(
		'ID, 
		post_type, 
		post_title, 
		post_date, 
		post_modified, 
		post_date_gmt, 
		post_modified_gmt, 
		post_status, 
		post_content, 
		post_type, 
		post_author', 
	0, 'post_modified DESC');


$list = [];
$c = 0;
// print_r($ovars);
// exit;
$rc = count($stories) + 1;

$factcheckterm = [];
$q = "SELECT term_id FROM `wp_terms` WHERE `slug` = 'fact-check'";
$r = $this->db->rawQuery($q);		
$factcheckterm['term_id'] = (!empty($r)) ? $r[0]['term_id'] : '';
// print_r($factcheckterm);
// exit;
if (!empty($factcheckterm['term_id'])) {
	$q = "SELECT term_taxonomy_id FROM `wp_term_taxonomy` WHERE `term_id` = '".$factcheckterm['term_id']."'";
	$r = $this->db->rawQuery($q);		
	$factcheckterm['term_taxonomy_id'] = (!empty($r)) ? $r[0]['term_taxonomy_id'] : '';
	// print_r($factcheckterm);
	// exit;	
} else {
	$factcheckterm['term_taxonomy_id'] = '';
}

foreach ($stories as $story) {



	$latest = $wptools->getLastRevisionForPost($story['ID']);
	$accepted = $wptools->getAcceptedRevisionForPost($story['ID']);


	if (!empty($latest)) {
		$article_state = $wptools->getPostMeta($latest['ID'], 'article_state');
	}

	if (empty($article_state)) {
		if ($story['post_type'] == 'projects') {
			$article_state = 'publish';
		} else {
			$article_state = 'draft';
		}
	}
	
	$q = "SELECT count(ID) as revision_count FROM `wp_posts` WHERE post_parent = $story[ID] and `post_date_gmt` > '$optvars[date_for_speed]' AND post_type = 'revision' AND post_name NOT LIKE '%autosave%' ";
	$r = $this->db->rawQuery($q);		
	$revisions_per_period = (!empty($r)) ? $r[0]['revision_count'] : 0;

	$q = "SELECT count(comment_ID) as comment_count FROM `wp_comments` WHERE comment_post_ID = $story[ID] and comment_date_gmt > '$optvars[date_for_speed]' AND comment_approved = 1";
	$r = $this->db->rawQuery($q);		
	$comments_per_period = (!empty($r)) ? $r[0]['comment_count'] : 0;
	
	$q = "SELECT comment_date FROM `wp_comments`  WHERE comment_post_ID = $story[ID] AND comment_approved = 1 ORDER by `comment_date_gmt` DESC LIMIT 1";
	$r = $this->db->rawQuery($q);		
	$last_comment_date = (!empty($r)) ? $r[0]['comment_date'] : '';

	
			
	if(($story['post_type'] == 'stories') && ($article_state == 'publish') && (!empty($accepted))) {
       $q = "SELECT count(ID) as pending_revisions_count FROM `wp_posts` WHERE post_parent = $story[ID] AND `post_date_gmt` > '$accepted[post_date_gmt]' AND ID != $accepted[ID] AND post_type = 'revision' AND post_name NOT LIKE '%autosave%'  ";
        $r = $this->db->rawQuery($q);  
       	$pending_revisions_count = $r[0]['pending_revisions_count'];

        
    } else {
        $pending_revisions_count = 0;
    }

   $q = "SELECT count(post_id) as pending_revisions_count FROM `wp_postmeta` WHERE `meta_key` = '_wt_is_pending_revision_for' AND `meta_value` = ".$story['ID'];
    $r = $this->db->rawQuery($q);  
   	$pending_revisions_count_meta = $r[0]['pending_revisions_count'];
   	// echo $pending_revisions_count_meta;
   	// exit;

    // if ($story['ID'] == 87424) {
    // 	print_r($q);
    // 	echo '<p>'.$pending_revisions_count.'</p>'; 
    // 	print_r($accepted);
    // 	exit;
    // }
	if ($article_state == 'publish') {
		if (!empty($accepted)) {
	  		$newtab_date =  $accepted['post_date_gmt'];
	  	} else {
	  		$newtab_date =  $story['post_date_gmt'];
	  	}
	} else {
		if (strlen($story['post_content']) >= $optvars['min_length_of_content_for_new']) {
			$newtab_date = $story['post_date_gmt'];
		} else {
			$newtab_date = "0000-00-00";
		}
		
	}
	
	$talktab_date = $last_comment_date;
	$worktab_date = $story['post_modified_gmt'];
	$attention = [];

	if ($pending_revisions_count > 0) {
		$attention[] = 'has_pending';
	}
	if ($pending_revisions_count_meta > 0) {
		$attention[] = 'has_pending_meta';
	}


	$q = "SELECT * FROM `wp_term_relationships` WHERE `term_taxonomy_id` = '".$factcheckterm['term_taxonomy_id']."' AND object_id = $story[ID]";
	$r = $this->db->rawQuery($q);		
	if (!empty($r)) {
		$attention[] = 'is_factcheck';
	}

	$list[] = [
		'post_id'					=>	$story['ID'],
		'newtab_date'				=>	$newtab_date,
		'talktab_date'				=>	$talktab_date,
		'worktab_date'				=>	$worktab_date,
		'comments_per_period'		=>	$comments_per_period ,
		'revisions_per_period'		=>	$revisions_per_period,
		'attention'					=>	$attention
	];

		
}

// krsort($list);

echo count($list).' remain from '.count($stories)."\n";
 // print_r($list);
// exit;


echo "Writing temporary chart\n";
foreach ($list as $l) {
    $log_id = $this->db->insert('_'.$tablename, array(
        	'post_id' 			=> $l['post_id'],
			'newtab_date'		=> $l['newtab_date'],
			'talktab_date'		=> $l['talktab_date'],
			'worktab_date'		=> $l['worktab_date'],
			'speed_comments'	=> $l['comments_per_period'],
			'edit_speed'		=> $l['revisions_per_period'],
			'attention'			=> implode(', ', $l['attention']),
    ));
	if ($this->db->getLastErrno() === 0) {
	    // echo 'Update succesfull';
	


	} else {
	    echo 'Update failed. Error: '.$this->db->getLastError()."\n";
	}
}   
  

echo "Task complete\n";
