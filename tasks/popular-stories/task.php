<?php 
require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);

/*
 * Gather own hits

 * Google Analytics
 * Number of comments
 * Facebook likes?
 * 
 */

$weights = array(
    'read' => 100
);


function createPSTable($dbo, $preload = false, $onlyifnotexists = true) {
    $tablename = (!$preload) ? 'dc_popular_stories' : '_dc_popular_stories';
    
    if ($onlyifnotexists) {
        $q = "SELECT *
        FROM information_schema.tables
        WHERE table_schema = 'wikitribune'
            AND table_name = '$tablename'
        LIMIT 1;";
        
        $r = $dbo->rawQuery($q);
        if (!empty($r)) {
            return false;
        }
        
    }
    $cq = array(
        "CREATE TABLE `$tablename` (
            `ps_id` bigint(20) NOT NULL,
            `post_id` bigint(20) NOT NULL,
            `rank_position` bigint(20) NOT NULL,
            `post_reads` int(11) NOT NULL,
            `points` int(11) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=".DB_CHARSET.";",
        "ALTER TABLE `$tablename`
  ADD PRIMARY KEY (`ps_id`),
  ADD KEY `post_id` (`post_id`);",
        "ALTER TABLE `$tablename`
  MODIFY `ps_id` bigint(20) NOT NULL AUTO_INCREMENT;",
        "COMMIT;"
        
    );
    foreach ($cq as $q) {
        $dbo->rawQuery($q);
    }
}


createPSTable($this->db);
createPSTable($this->db, true);


$since = date("Y-m-d H:i:s", strtotime("-1 year"));
$q = "SELECT l.*, p.post_date_gmt, p.post_date FROM `dc_post_read_log` l 
LEFT JOIN wp_posts p ON l.post_id = p.ID 
WHERE 
    date_read > '$since' 
    AND 
    p.post_type = 'stories'
";
$r = $this->db->rawQuery($q);
echo count($r)." story reads to process\n";
$bypoints = array();
$bypostid = array();
$t = time();
echo "Assigning points\n";
foreach ($r as $post) {
    if (empty($post['post_date_gmt'])) {
        $dtu = $post['post_date_gmt'];
    } else {
        $dtu = $post['post_date'];
    }
    
    $published_ago = $t - strtotime($dtu);
    
    if (empty($bypostid[$post['post_id']]['total_read_count'])) {
        $bypostid[$post['post_id']]['total_read_count'] = $post['read_count'];
    } else {
        $bypostid[$post['post_id']]['total_read_count'] += $post['read_count'];
    }
    
    $daysago = round(($t - strtotime($post['date_read'])) / 86400);
    
    $points = ($weights['read'] * $post['read_count']);
    if ($published_ago > 0) {
        if ($daysago > 0) {
            $div = $published_ago + $daysago;
        } else {
            $div = $published_ago;
        }
        $div =  $div / 100000;
        $points = round($points / ($div));
    }
    if  ($points > 0) {
        $bypoints[$post['post_id']] = (empty($bypoints[$post['post_id']])) ?
            $points :
            $points + $bypoints[$post['post_id']];
    }
}
echo count($bypostid)." stories were read\n";
arsort($bypoints);
$rank = 1;
echo "Writing temporary chart\n";
foreach ($bypoints as $post_id => $points) {
    $total = (isset($bypostid[$post_id]['total_read_count'])) ? $bypostid[$post_id]['total_read_count'] : 0;
    $ps_log_id = $this->db->insert('_dc_popular_stories', array(
        'post_id'         =>  $post_id,
        'rank_position'   =>  $rank,
        'post_reads'      =>  $total,
        'points'          =>  $points
    ));
    /*
    if (empty($log_id)) {
        echo $this->db->getLastError()."\n";
    } else {
        echo $log_id."\n";
    }
    */
    $rank++;
}   
echo "Swapping tables\n";
$this->db->rawQuery("DROP TABLE `dc_popular_stories`");
$this->db->rawQuery("RENAME TABLE `_dc_popular_stories` TO `dc_popular_stories`;");
?>