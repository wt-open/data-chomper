<?php

$weights = array (
    'projects_authored'  =>  10,
    'stories_authored'  =>  10,
    'edited'            =>  5,
    'commented'         =>  2,
);


require_once   $this->toolsfilepath.'wptools.php';
$wptools = new wptools($this->db);



function createUSTable($dbo, $preload = false, $onlyifnotexists = true) {
    $tablename = (!$preload) ? 'dc_user_stats' : '_dc_user_stats';
    
    if ($onlyifnotexists) {
        $q = "SELECT *
        FROM information_schema.tables
        WHERE table_schema = 'wikitribune'
            AND table_name = '$tablename'
        LIMIT 1;";
        
        $r = $dbo->rawQuery($q);
        if (!empty($r)) {
            return false;
        }
        
    }
    $cq = array(
        "CREATE TABLE `$tablename` (
  `stat_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `rank_position` bigint(20) NOT NULL,
  `total_published_stories_authored` int(11) NOT NULL,
  `total_user_edits` int(11) NOT NULL,
  `total_comments_made` int(11) NOT NULL,
  `total_published_projects_authored` int(11) NOT NULL,
  `total_comment_length` int(11) NOT NULL,
  `total_story_length` int(11) NOT NULL,
  `avg_story_length` double(13,2) NOT NULL,
  `avg_comment_length` double(13,2) NOT NULL,
  `timespan_weeks` double(7,2) NOT NULL,
  `frequency_graph` longtext NOT NULL,
  `total_contributions` int(11) NOT NULL,
  `total_points` bigint(20) NOT NULL,
  `score` int(10) NOT NULL,
  `avg_contributions_per_week` double(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=".DB_CHARSET.";
",
        "ALTER TABLE `$tablename`
  ADD PRIMARY KEY (`stat_id`),
  ADD UNIQUE KEY `rank_position` (`rank_position`),
  ADD KEY `user_id` (`user_id`);",
        "ALTER TABLE `$tablename`
  MODIFY `stat_id` bigint(20) NOT NULL AUTO_INCREMENT;",
        "COMMIT;"
        
    );
    foreach ($cq as $q) {
        $dbo->rawQuery($q);
    }
}
createUSTable($this->db);
createUSTable($this->db, true);

$q = 'SELECT count(ID) as total_no_users FROM wp_users ';
$r = $this->db->rawQuery($q);
$total_number_of_users = $r[0]['total_no_users'];

function frequency_graph_base ($start_year, $start_month) {
    $end_Y = date("Y");
    $end_m = date("m");
    $frequency_graph = array();
    
    $y = $start_year;
    $m = $start_month;
    
    while($y <= $end_Y) {
        while ($m <= 12) {
            $frequency_graph[$y.'-'.sprintf('%02d', $m)] = array(
                'stories_authored'=>0,
                'revisions_made'=>0,
                'comments'=>0
            );
            if (($end_Y == $y) && ($end_m == $m)) {
                break; 
            }
            $m++;
        }
        $y++;
        $m = 1;
    }
    return($frequency_graph);
}
// Work out STORY contributions by author ============================================

$q = 'SELECT 
    count(post_author) as user_authored, 
    sum(CHAR_LENGTH(post_content)) as story_length, 
    post_author, 
    user_registered,
    YEAR(post_date) as s_year, 
    MONTH(post_date) as s_month
FROM wp_posts p
LEFT JOIN wp_users u on post_author = u.ID 
WHERE  
    p.post_type = "stories"  
    AND p.post_status = "publish"
    AND u.ID IS NOT NULL   
    GROUP by post_author, YEAR(post_date), MONTH(post_date)
';
$number_of_stories_by_author = $this->db->rawQuery($q);

$statistics = array();
foreach ($number_of_stories_by_author as $a) {
    if (empty($statistics[$a['post_author']]['total_published_stories_authored'])) {
        $statistics[$a['post_author']]['total_published_stories_authored'] = 0;
    }
    if (empty($statistics[$a['post_author']]['total_story_length'])) {
        $statistics[$a['post_author']]['total_story_length'] = 0;
    }
    if (!isset($statistics[$a['post_author']]['frequency_graph'])) {
        $d = explode('-',$a['user_registered']);
        if (!isset($d[1])) {
            continue;
        }
        $statistics[$a['post_author']]['frequency_graph'] = frequency_graph_base($d[0], $d[1]);
    }
    $statistics[$a['post_author']]['frequency_graph'][$a['s_year'].'-'.sprintf('%02d',$a['s_month'])]['stories_authored'] = $a['user_authored'];
    $statistics[$a['post_author']]['user_registered'] = $a['user_registered'];
    $statistics[$a['post_author']]['total_published_stories_authored'] += $a['user_authored'];
    $statistics[$a['post_author']]['total_story_length'] += $a['story_length'];
}

// Work out PROJECT contributions by author ============================================

$q = 'SELECT count(post_author) as user_authored, sum(CHAR_LENGTH(post_content)) as project_length, post_author, user_registered
FROM wp_posts p
LEFT JOIN wp_users u on post_author = u.ID
WHERE
    p.post_type = "projects"
    AND p.post_status = "publish"
    AND u.ID IS NOT NULL
    GROUP by post_author
';
$number_of_projects_by_author = $this->db->rawQuery($q);


foreach ($number_of_projects_by_author as $a) {
    $statistics[$a['post_author']]['total_published_projects_authored'] = $a['user_authored'];
    if (!isset($statistics[$a['post_author']]['user_registered'])) {
        $statistics[$a['post_author']]['user_registered'] = $a['user_registered'];
    }
}

// Work out contributions by REVISIONS to others ===============================
$q = 'SELECT
    count(p.post_author) as user_edits,
    p.post_author,
    user_registered,
    YEAR(p.post_date) as r_year,
    MONTH(p.post_date) as r_month
FROM wp_posts p
LEFT JOIN wp_users u on post_author = u.ID
LEFT JOIN wp_posts pp on p.post_parent = pp.ID
WHERE
    p.post_type = "revision"
    and p.post_name  NOT LIKE "%-autosave-%"
    and pp.post_author != p.post_author
    AND u.ID IS NOT NULL
GROUP by post_author, YEAR(p.post_date), MONTH(p.post_date)
';
$number_of_revisions_by_author = $this->db->rawQuery($q);

foreach ($number_of_revisions_by_author as $a) {
    if (empty($statistics[$a['post_author']]['total_user_edits'])) {
        $statistics[$a['post_author']]['total_user_edits'] = 0;
    }
    if (!isset($statistics[$a['post_author']]['frequency_graph'])) {
        $d = explode('-',$a['user_registered']);
        if (!isset($d[1])) {
            continue;
        }
        $statistics[$a['post_author']]['frequency_graph'] = frequency_graph_base($d[0], $d[1]);
    }
    
    $statistics[$a['post_author']]['frequency_graph'][$a['r_year'].'-'.sprintf('%02d',$a['r_month'])]['revisions_made'] = $a['user_edits'];
    
    
    $statistics[$a['post_author']]['total_user_edits'] += $a['user_edits'];
    if (!isset($statistics[$a['post_author']]['user_registered'])) {
        $statistics[$a['post_author']]['user_registered'] = $a['user_registered'];
    }
}


// Work out contributions by COMMENTS ============================================

$q = 'SELECT
    count(user_id) as user_commented,
    user_id, sum(CHAR_LENGTH(comment_content)) as comment_length,
    user_registered,
    YEAR(comment_date) as c_year,
    MONTH(comment_date) as c_month
FROM `wp_comments`
LEFT JOIN wp_posts p ON comment_post_ID = p.ID
LEFT JOIN wp_users u on user_id = u.ID
WHERE `comment_approved` = 1
    AND (p.post_type = "stories" OR p.post_type = "projects")
    AND user_id > 0 AND u.ID IS NOT NULL
    AND p.post_status = "publish"
    GROUP by  user_id, YEAR(comment_date), MONTH(comment_date)
';
$number_of_comments_by_author = $this->db->rawQuery($q);

foreach ($number_of_comments_by_author as $a) {
    if (empty($statistics[$a['user_id']]['total_comments_made'])) {
        $statistics[$a['user_id']]['total_comments_made'] = 0;
    }
    if (empty($statistics[$a['user_id']]['total_comment_length'])) {
        $statistics[$a['user_id']]['total_comment_length'] = 0;
    }
    if (!isset($statistics[$a['user_id']]['frequency_graph'])) {
        $d = explode('-',$a['user_registered']);
        if (!isset($d[1])) {
           continue;
        }
        $statistics[$a['user_id']]['frequency_graph'] = frequency_graph_base($d[0], $d[1]);
    }
    $statistics[$a['user_id']]['total_comments_made'] += $a['user_commented'];
    $statistics[$a['user_id']]['total_comment_length'] += $a['comment_length'];
    $statistics[$a['user_id']]['frequency_graph'][$a['c_year'].'-'.sprintf('%02d',$a['c_month'])]['comments'] = $a['user_commented'];
    if (!isset($statistics[$a['user_id']]['user_registered'])) {
        $statistics[$a['user_id']]['user_registered'] = $a['user_registered'];
    }
}


// Work out what's left ============================================


$chart = array();
$x = 1;
$now = time();
foreach ($statistics as $user_id => $s) {
    
    $total = 0;
    $totalcont = 0;
    
    if (!isset($statistics[$user_id]['total_story_length'])) {
        $statistics[$user_id]['total_story_length'] = 0;
    }
    if (!isset($statistics[$user_id]['total_comments_made'])) {
        $statistics[$user_id]['total_comments_made'] = 0;
    }
    if (isset($statistics[$user_id]['total_published_stories_authored'])) {
        $statistics[$user_id]['avg_story_length'] = round($s['total_story_length'] / $s['total_published_stories_authored'],2);
        $total += $statistics[$user_id]['total_published_stories_authored'] * $weights['stories_authored'];
        $totalcont += $statistics[$user_id]['total_published_stories_authored'];
    } else {
        $statistics[$user_id]['total_published_stories_authored'] = 0;
        $statistics[$user_id]['avg_story_length'] = 0;
    }
    
    if (isset($statistics[$user_id]['total_published_projects_authored'])) {
        $total += $statistics[$user_id]['total_published_projects_authored'] * $weights['projects_authored'];
        $totalcont += $statistics[$user_id]['total_published_projects_authored'];
    } else {
        $statistics[$user_id]['total_published_projects_authored'] = 0;
    }
    if (isset($statistics[$user_id]['total_user_edits'])) {
        $total += $statistics[$user_id]['total_user_edits']  * $weights['edited'];
        $totalcont += $statistics[$user_id]['total_user_edits'];
    } else {
        $statistics[$user_id]['total_user_edits'] = 0;
    }
    
    if (isset($statistics[$user_id]['total_comment_length'])) {
        $statistics[$user_id]['avg_comment_length'] = round($s['total_comment_length'] / $s['total_comments_made'],2);
        $total += $statistics[$user_id]['total_comments_made'] * $weights['commented'];;
        $totalcont += $statistics[$user_id]['total_comments_made'];
    } else {
        $statistics[$user_id]['total_comment_length'] = 0;
        $statistics[$user_id]['avg_comment_length'] = 0;
    }
    
   
    
    
    $then = strtotime($s['user_registered']);
    $timespan = $now - $then;
    $timespan_weeks = round($timespan / 604800,1);
    $statistics[$user_id]['total_points'] = $total;
    $score = ($timespan_weeks > 0) ? 
        round($total / ($timespan_weeks / 4)) : 
        round($total);
    
    $statistics[$user_id]['total_contributions'] = $totalcont;
    $statistics[$user_id]['score'] = $score;
    
    $statistics[$user_id]['timespan_weeks'] = $timespan_weeks;
    $statistics[$user_id]['avg_contributions_per_week'] = 
        ($timespan_weeks > 0) ? 
        round($totalcont / $timespan_weeks,2) :
        0;
    
    if (!isset($statistics[$user_id]['frequency_graph'])) {
        $d = explode('-',$s['user_registered']);
        if (!isset($d[1])) {
            continue;
        }
        $statistics[$user_id]['frequency_graph'] = frequency_graph_base($d[0], $d[1]);
    }
    $chart[$score.'.'.$x] = $user_id;
    $x++;
}
krsort($chart);
$newstats = array();
$x = 1;
$e = count($chart);
// $this->db->rawQuery("TRUNCATE TABLE `dc_user_stats`");


foreach ($chart as $user_id) {
    if (isset($statistics[$user_id])) {
        $newstats[$user_id] = $statistics[$user_id];
        $newstats[$user_id]['rank_position'] = $x;
        $q = "INSERT INTO `_dc_user_stats` 
        ( 
        `user_id`, 
        `rank_position`, 
        `total_published_stories_authored`, 
        `total_user_edits`, 
        `total_comments_made`, 
        `total_published_projects_authored`, 
        `total_comment_length`, 
        `total_story_length`, 
        `avg_story_length`, 
        `avg_comment_length`, 
        `timespan_weeks`, 
        `frequency_graph`, 
        `total_contributions`, 
        `total_points`, 
        `score`, 
        `avg_contributions_per_week`
    ) VALUES (
        '$user_id', 
        '$x', 
        '".$newstats[$user_id]['total_published_stories_authored']."', 
        '".$newstats[$user_id]['total_user_edits']."', 
        '".$newstats[$user_id]['total_comments_made']."', 
        '".$newstats[$user_id]['total_published_projects_authored']."', 
        '".$newstats[$user_id]['total_comment_length']."', 
        '".$newstats[$user_id]['total_story_length']."', 
        '".$newstats[$user_id]['avg_story_length']."', 
        '".$newstats[$user_id]['avg_comment_length']."', 
        '".$newstats[$user_id]['timespan_weeks']."', 
        '".json_encode($newstats[$user_id]['frequency_graph'])."', 
        '".$newstats[$user_id]['total_contributions']."', 
        '".$newstats[$user_id]['total_points']."', 
        '".$newstats[$user_id]['score']."', 
        '".$newstats[$user_id]['avg_contributions_per_week']."'
    );";
        $this->db->rawQuery($q);
        unset($statistics[$user_id]);
    } else {
        // Huh?
    }
    $x++;
}
echo "Swapping tables\n";
$this->db->rawQuery("DROP TABLE `dc_user_stats`");
$this->db->rawQuery("RENAME TABLE `_dc_user_stats` TO `dc_user_stats`;");


echo count($newstats).'/'.$total_number_of_users.' users made contributions';
echo "\nTask complete\n";
