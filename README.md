*** Congratulations on the purchase of your brand new Data-Chomper.  We hope it will provide fun for all the family for years to come. ***

This mini application has been designed to work as a traditional site but also without the traditional front end.
It's job is to run large queries and provide it's workings as a results table in the database.


=== Installation ===

1. Copy sample.conf.php to conf.php 

    Copy the file and change the variables in the section that matches your environment, ie: local, dev, staging, master.

    These will be the same as in config.php of wordress.  Alternatively create a new user but make sure that the new user has table creation and rennaming permissions.

2. Run composer install

3. (Optional - Local) set the localhost to the "public" dir

    If running through the browser you need to point the localhost to this folder so that only public files are available.
    You can then run the tasks by clicking the links.
    THERE IS NO PASSWORD - YET so not good for production unless we use HTAUTH

4. Setup the cron or server task to process the task:

	php datachomper {ENV} {TASK}

	eg:

	php datachomper local contributor-stats
	php datachomper local popular-stories
	php datachomper local related-stories
	php datachomper local user-stats

	NOTE
	To run all the tasks in the order they have been set - run the following:

	php datachomper {ENV} _all-tasks
	ie 
	php datachomper local _all-tasks



