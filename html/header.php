<!DOCTYPE html>
<html lang="en-GB">
    <head>
    	<title>DataChomper</title>
    	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    	<style>
    	body {
    	   color: #9fA6A6;
    	   /* http://www.noisetexturegenerator.com/ */
    	   background-color: #1f2626;
    	   background-image: url(/assets/noisy-texture-100x100-o3-d18-c-1f2626-t1.png);
    	  }
    	a {
    	border-bottom: 1px dotted #0F0;
    	color: #0F0;
    	}
    	li.active a {
    	   color: yellow;
    	}
    	pre {
    	font-family: monospace; 
    	font-size: 1.2rem; 
    	background-color: #101910; 
    	color: #0F0; 
    	padding: 20px; 
    	height: 80vh; 
    	overflow-y: scroll;
    	}
    	h1,h2,h3,h4,h5 {
    	
    	   opacity: 0.5;
    	   color: #888;
    	   text-shadow: 0px 1px 0px #FFF;
    	}
    	h3,h4 {
    	   margin-top: .5em;
    	   padding: .5em 0;
    	} 
    	h3 {
    	   border-top: 2px solid #0F0;
    	   border-bottom: 2px solid #0F0;
    	}
    	h4 {
    	   border-top: 1px dashed #0F0;
    	   border-bottom: 1px dashed #0F0;
    	}
    	</style>
    </head>
    <body>
    	<div class="container">
    		<div class="row">
    			<div class="col-md-4">
            		<h1>WikiTribune :: DataChomper :: Local</h1>
            		<div class="nav">
            		<ul>
                    	<?php 
                    	foreach ($this->getTaskList() as $t => $task) {
                    	    $ac = ($t == $this->activetask) ? ' class="active"' : '';
                    	    
                    	    echo '<li'.$ac.'><a href="?task='.$t.'">'.$task->name.' ['.$task->maxtime.']</a></li>';
                        }
                        ?>  
                        <li><a href="?task=_all-tasks" style="color: red;">[!] All Tasks [!]</a></li>
                    	</ul>
                    	</div>
    			</div>

    	