<?php
$verbose = ((isset($argv[3])) && ($argv[3] == 'verbose')) ? true : false;
if ($verbose) { 
echo "\n\n ";
echo "
__________________________________________________
--------------------------------------------------

            * W I K I T R I B U N E *
                      __      ___
                     / o\    /o o\
            DATA    | C < HO | M |PER
                     \__/    |,,,|

__________________________________________________
--------------------------------------------------

";
} else {
    echo 'DataChomper: ';
}
if (!empty($argv[1])) {
    $allowed = array('local', 'dev','staging','master');
    if (!in_array($argv[1], $allowed)) {
        die("That environment is not valid");
    } else {
        define("DC_ENV", $argv[1]);
    }
}
include ("main.php");

if ((!empty($argv[1])) && (!empty($argv[2]))) {
    echo "Running task [ $argv[2] ]...\n\n";
    $WTDC = new DataChomper($dboc, $argv[2]);
    if ($verbose) { 
        echo $WTDC->unfilteredTaskOutput();
    }
} else if ((!empty($argv[1])) && (empty($argv[2]))) {
    $WTDC = new DataChomper($dboc);
    echo "Please specify an argument for a task...\n\n";
    foreach ($WTDC->getTaskList() as $k => $t) {
        echo 'Add '.$k.' to run the '.$t->name." task\n";
    }
} else {
    die("Please specify an environment and a task");
}
if ($verbose) { 
    die("\n\n----------DONE-------------\n\n");
} else {
    echo 'Done';
}